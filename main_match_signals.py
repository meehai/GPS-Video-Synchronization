import matplotlib.pyplot as plt
from argparse import ArgumentParser
import mplleaflet
import numpy as np

from signals.signal_smoothing import savitzky_golay
from signals.signal_matching_try1 import matchSignals
from signals.utils import plot_signal
from gps.utils import syncFrequencies, getGPSSpeed
from main_export_gps import getKML

from scipy.signal import medfilt

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("flow_path")
	parser.add_argument("altitude_path")
	parser.add_argument("location_path")

	parser.add_argument("--plot_signals", type=int, default=0)
	parser.add_argument("--offset", type=int, default=None)
	parser.add_argument("--offset_start", type=int, default=None, help="Only when --offset is set for metadata.")
	parser.add_argument("--offset_end", type=int, default=None, help="Only when --offset is set for metadata.")
	parser.add_argument("--export_gps", type=str, default=None)
	parser.add_argument("--flow_frequency", type=float, default=29.97)
	args = parser.parse_args()

	args.plot_signals = bool(args.plot_signals)
	return args

def minMax(x):
	min, max = np.min(x), np.max(x)
	x -= min
	x /= (max - min)
	return x

def getGpsDataFromFile(file, desiredFreq, useMedianFilter=True):
	gpsData = np.load(file)
	frequency = np.median(1 / (gpsData[1 : , 2] - gpsData[0 : -1, 2]))
	lat, lon = gpsData[..., 0], gpsData[..., 1]

	meanLat, stdLat = np.mean(lat), np.std(lat, axis=0)
	meanLon, stdLon = np.mean(lon), np.std(lon, axis=0)
	print("Mean lat", meanLat, "Std lat", stdLat, "Mean lon", meanLon, "Std long", stdLon, "Frequency", frequency)
	lat = syncFrequencies(lat, currentFreq=frequency, desiredFreq=desiredFreq)
	lon = syncFrequencies(lon, currentFreq=frequency, desiredFreq=desiredFreq)
	meanLat, stdLat = np.mean(lat), np.std(lat, axis=0)
	meanLon, stdLon = np.mean(lon), np.std(lon, axis=0)
	print("After sync. Mean lat", meanLat, "Std lat", stdLat, "Mean lon", meanLon, "Std long", stdLon)

	if useMedianFilter:
		kernel_size = lat.shape[0] // 100 + ((lat.shape[0] // 100) % 2 == 0)
		# plot_signal(minMax(lat))
		lat = medfilt(lat, kernel_size)
		# plot_signal(minMax(lat))

		# plt.figure()
		# plot_signal(minMax(lon))
		lon = medfilt(lon, kernel_size)
		# plot_signal(minMax(lon))
		# show_plots()

	gpsData = np.concatenate([np.expand_dims(lat, axis=-1), np.expand_dims(lon, axis=-1)], axis=-1)
	return gpsData

def getGpsSpeedFromFile(file, desiredFreq):
	gpsData = np.load(file)
	gpsSpeed = getGPSSpeed(gpsData)
	mean, std = np.mean(gpsSpeed), np.std(gpsSpeed)
	frequency = np.median(1 / (gpsData[1 : , 2] - gpsData[0 : -1, 2]))
	gpsSpeed = np.clip(gpsSpeed, 0, mean + 3 * std)

	mean, std = np.mean(gpsSpeed), np.std(gpsSpeed)
	print("Mean speed", mean, "Std speed", std, "Frequency", frequency)
	gpsSpeed = syncFrequencies(gpsSpeed, currentFreq=frequency, desiredFreq=desiredFreq)
	mean, std = np.mean(gpsSpeed), np.std(gpsSpeed)
	print("After sync. Mean speed", mean, "Std speed", std)

	window_size = len(gpsSpeed) // 50 + ((len(gpsSpeed) // 50) % 2 == 0)
	filtered_gpsSpeed = savitzky_golay(gpsSpeed, window_size, 3)
	# plot_signal(flow, "Original flow")
	# plot_signal(filtered_flow, "Filtered flow")
	# plt.legend()

	speed = minMax(filtered_gpsSpeed)
	return speed

def getFlowFromFile(file):
	flowData = np.load(file)

	# flowV = flowData[..., 1]
	flowV = np.hypot(flowData[..., 0], flowData[..., 1])
	mean, std = np.mean(flowV), np.std(flowV)
	flowV = np.clip(flowV, mean - 3 * std, mean + 3 * std)

	window_size = len(flowV) // 50 + ((len(flowV) // 50) % 2 == 0)
	filtered_flow = savitzky_golay(flowV, window_size, 3)
	# plot_signal(flow, "Original flow")
	# plot_signal(filtered_flow, "Filtered flow")
	# plt.legend()

	flow = minMax(filtered_flow)
	return flow

def getGpsAltitudeFromFile(file, desiredFreq):
	gpsData = np.load(file)
	gpsAltitude = gpsData[..., 0]
	mean, std = np.mean(gpsAltitude), np.std(gpsAltitude)
	frequency = np.median(1 / (gpsData[1 : , 1] - gpsData[0 : -1, 1]))
	# gpsAltitude = np.clip(gpsAltitude, 0, mean + 3 * std)

	mean, std = np.mean(gpsAltitude), np.std(gpsAltitude)
	print("Mean altitude", mean, "Std altitude", std, "Frequency", frequency)
	gpsAltitude = syncFrequencies(gpsAltitude, currentFreq=frequency, desiredFreq=desiredFreq)
	mean, std = np.mean(gpsAltitude), np.std(gpsAltitude)
	print("After sync. Mean altitude", mean, "Std altitude", std)

	altitude = minMax(gpsAltitude)
	return altitude

def main():
	args = getArgs()

	flow = getFlowFromFile(args.flow_path)
	altitude = getGpsAltitudeFromFile(args.altitude_path, args.flow_frequency)
	speed = getGpsSpeedFromFile(args.location_path, args.flow_frequency)
	print("Flow", flow.shape, "Altitude", altitude.shape, "Speed", speed.shape)

	origFlow = np.copy(flow)
	origAltitude = np.copy(altitude)
	if not args.offset is None:
		print("Offset manually put at", args.offset)
		# speed = speed[args.offset : args.offset + len(flow)]
		speed = speed[args.offset : args.offset + len(flow)]
		altitude = altitude[args.offset : args.offset + len(flow)]

	if not args.offset_start is None:
		altitude = altitude[args.offset_start: ]
		flow = flow[args.offset_start: ]
		speed = speed[args.offset_start: ]
		if not args.offset_end is None:
			args.offset_end -= args.offset_start

	if not args.offset_end is None:
		altitude = altitude[0 : args.offset_end]
		flow = flow[0 : args.offset_end]
		speed = speed[0 : args.offset_end]

	print("Flow", flow.shape, "Altitude", altitude.shape, "Speed", speed.shape)
	if args.plot_signals:
		plt.figure()
		plot_signal(altitude, label="Altitude")
		plot_signal(flow, label="Flow")
		plot_signal(speed, label="GPS Speed")
		plt.legend()
		plt.show()

	if args.export_gps:
		assert (not args.offset is None) and (not args.offset_start is None) and (not args.offset_end is None)
		gpsData = getGpsDataFromFile(args.location_path, args.flow_frequency)
		gpsData = gpsData[args.offset : args.offset + len(origFlow)]
		origAltitude = origAltitude[args.offset : args.offset + len(origFlow)]
		npData = np.concatenate([gpsData, np.expand_dims(origAltitude, axis=-1)], axis=-1)
		print("Saving synced GPS & Altitude data of shape", npData.shape, "to", args.export_gps)
		np.save(args.export_gps, npData)

		kml = getKML(gpsData)
		kmlFile = args.export_gps + ".kml"
		g = open(kmlFile, "w")
		print("Saving KML file to", kmlFile)
		g.write(kml)

		plt.plot(gpsData[..., 1], gpsData[..., 0])
		osmHtmlFile = args.export_gps + ".html"
		print("Saving OSM html file to", osmHtmlFile)
		mplleaflet.save_html(fileobj=osmHtmlFile)

		metaFile = "%s_meta.txt" % (args.export_gps)
		print("Saving metadata (offsets) to", metaFile)
		f = open(metaFile, "w")
		f.write("Offset sync: %d\nOffset start (after sync): %d\nOffset end (after sync): %d" \
			% (args.offset, args.offset_start, args.offset_end))
		f.flush()

if __name__ == "__main__":
	main()
