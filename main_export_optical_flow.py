# Script used to extract optical flow features from a video.
import sys
import pims
import numpy as np
from neural_wrappers.utilities import resize_batch
from argparse import ArgumentParser

def npGetInfo(data):
	return "Shape: %s. Min: %s. Max: %s. Mean: %s. Std: %s. Dtype: %s" % \
		(data.shape, np.min(data), np.max(data), np.mean(data), np.std(data), data.dtype)

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("in_file")
	parser.add_argument("out_file")
	parser.add_argument("--flow_lookahead", type=int, default=1)
	parser.add_argument("--batch_size", type=int, default=15)

	args = parser.parse_args()
	assert args.flow_lookahead > 0
	return args

def doPwcNet(rgb, rgb_other):
	sys.path.append("/home/mihai/Public/Projects/optical-flow/pytorch-pwc")
	from inference import runImages

	assert rgb.shape == rgb_other.shape, "%s vs %s" % (rgb.shape, rgb_other.shape)
	if rgb.shape[0] != 480:
		height = 480
		width = int(480 / (rgb.shape[1] / rgb.shape[2]))
		rgb = resize_batch(rgb, (height, width, 3), "bilinear")
		rgb_other = resize_batch(rgb_other, (height, width, 3), "bilinear")

	res = runImages(rgb, rgb_other)
	u, v = res
	return u, v

def main():
	args = getArgs()

	video = pims.Video(args.in_file)
	flowLookahead = args.flow_lookahead
	N = len(video) - flowLookahead
	result = np.zeros((N, 2), dtype=np.float32)
	batchSize = args.batch_size

	numSteps = N // batchSize + (N % batchSize != 0)
	j = 0
	for i in range(numSteps):
		startIndex, endIndex = (i * batchSize), min(N, (i + 1) * batchSize)
		# print(startIndex, endIndex, startIndex + flowLookahead, endIndex + flowLookahead)

		firstFrames = np.array(video[startIndex : endIndex])
		secondFrames = np.array(video[startIndex + flowLookahead : endIndex + flowLookahead])
		u, v = doPwcNet(firstFrames, secondFrames)
		flowUSum = np.sum(u, axis=(1, 2))
		flowVSum = np.sum(v, axis=(1, 2))
		res = np.concatenate([np.expand_dims(flowUSum, axis=-1), np.expand_dims(flowVSum, axis=-1)], axis=-1)
		result[startIndex : endIndex] = res
		for j in range(len(u)):
			print("%d/%d: U:%2.3f V:%2.3f Hypot:%2.3f" % ((i * batchSize) + j, N, \
				flowUSum[j], flowVSum[j], np.sum(np.hypot(u[j], v[j]))))
			# plot_image(firstFrames[j], axis=(1, 4, 1), new_figure=True)
			# plot_image(secondFrames[j], axis=(1, 4, 2), new_figure=False)
			# plot_image(u[j], cmap="hot", axis=(1, 4, 3), new_figure=False)
			# plot_image(v[j], cmap="hot", axis=(1, 4, 4), new_figure=False)
			# show_plots()
	np.save(args.out_file, result)
	print("Finished", npGetInfo(result))

if __name__ == "__main__":
	main()
