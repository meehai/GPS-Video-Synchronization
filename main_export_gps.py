import sys
import numpy as np
import mplleaflet
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from io import StringIO
import pandas as pd

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("in_file")
	parser.add_argument("out_file")
	parser.add_argument("gps_format")
	parser.add_argument("--export_kml", default=0, type=int, help="If set to 1, will also export a .kml file")
	parser.add_argument("--export_osm", default=0, type=int, help="If set to 1, will also export a .html file (OSM)")
	parser.add_argument("--frequency", type=float)

	args = parser.parse_args()
	args.export_kml = bool(args.export_kml)
	assert args.gps_format in ("standard_gnrmc", "standard_gpgga", "matrice_100_logs_nots", "matrice_100_logs", \
		"standard_gngga")
	if args.gps_format == "matrice_100_logs_nots":
		assert not args.frequency is None
	return args

def getDecDegree(val):
	if len(val) < 9:
		raise Exception

	if len(val) == 10 or val[0] == "0":
		val = val[1 : ]
	d = int(val[0 : 2])
	m = int(val[2 : 4])
	s = int(val[5 : ]) / (10**(len(val[5: ]) - 1)) * 6
	return d + m / 60 + s / 3600

def getKML(coords):
	Str = "<Placemark>\n\t<name>Untitled Path</name>\n\t<LineString>\n\t\t<tessellate>1</tessellate>\n\t\t" + \
		"<altitudeMode>relativeToGround</altitudeMode>\n\t\t<coordinates>\n\t\t\t"
	for i in range(len(coords)):
		Str += "%2.6f,%2.6f " % (coords[i, 1], coords[i, 0])
	Str += "\n\t\t</coordinates>\n\t</LineString>\n</Placemark>"
	return Str

def parseGPSDataGNRMC(rawGPSData):
	lines = list(filter(lambda x : x.startswith("$GNRMC"), rawGPSData))
	splitLines = list(map(lambda x : x.split(","), lines))
	splitLines = list(filter(lambda x : len(x) == 13 and x[1] != "" and x[2] != "" and x[3] != "" and \
		x[4] != "" and x[5] != "" and x[6] != "", splitLines))
	splitLines = list(map(lambda x : (x[1], x[2], x[3], x[4], x[5], x[6]), splitLines))

	newLines = []
	for i in range(len(splitLines)):
		line = splitLines[i]
		time, _, lat, lat_pos, lon, lon_pos = line

		if len(lat) not in (9, 10) or len(lon) not in (9, 10):
			continue

		try:
			lat_dd = getDecDegree(lat)
			lon_dd = getDecDegree(lon)
			time = float(time) 
		except Exception as e:
			print(str(e))
			continue
		if lat_pos == "S":
			lat_dd = 0 - lat_dd
		if lon_pos == "W":
			lon_dd = 0 - lon_dd

		newLines.append((lat_dd, lon_dd, time))

	newLines = np.array(newLines)
	return newLines

def parseGPSDataGPGGA(rawGPSData):
	lines = list(filter(lambda x : x.startswith("$GPGGA"), rawGPSData))
	splitLines = list(map(lambda x : x.split(","), lines))
	splitLines = list(filter(lambda x : len(x) == 15 and x[1] != "" and x[2] != "" and x[3] != "" and \
		x[4] != "" and x[5] != "", splitLines))
	splitLines = list(map(lambda x : (x[1], x[2], x[3], x[4], x[5]), splitLines))

	newLines = []
	for i in range(len(splitLines)):
		line = splitLines[i]
		time, lat, lat_pos, lon, lon_pos = line

		if len(lat) not in (9, 10) or len(lon) not in (9, 10):
			continue

		try:
			lat_dd = getDecDegree(lat)
			lon_dd = getDecDegree(lon)
			time = float(time)
		except Exception as e:
			print(str(e))
			continue
		if lat_pos == "S":
			lat_dd = 0 - lat_dd
		if lon_pos == "W":
			lon_dd = 0 - lon_dd

		newLines.append((lat_dd, lon_dd, time))

	newLines = np.array(newLines)
	return newLines

def parseGPSDataGNGGA(rawGPSData):
	splitLines = list(map(lambda x : x.split(","), rawGPSData))
	splitLines = list(filter(lambda x : len(x) == 15 and x[1] != "" and x[2] != "" and x[3] != "" and \
		x[4] != "" and x[5] != "", splitLines))
	splitLines = list(map(lambda x : (x[1], x[2], x[3], x[4], x[5], x[9]), splitLines))

	newLines = []
	for i in range(len(splitLines)):
		line = splitLines[i]
		time, lat, lat_pos, lon, lon_pos, alt = line

		try:
			lat_dd = getDecDegree(lat)
			lon_dd = getDecDegree(lon)
			time = float(time)
		except Exception as e:
			print("GNGGA Exception:", str(e))
			continue

		if lat_pos == "S":
			lat_dd = 0 - lat_dd
		if lon_pos == "W":
			lon_dd = 0 - lon_dd
		alt = float(alt)

		newLines.append((time, lat_dd, lon_dd, alt))

	newLines = np.array(newLines)
	return newLines

# def parseGPSMatrice100NoTs(rawGPSData, frequency):
# 	lines = list(map(lambda x : x.split(",")[1 : 3], rawGPSData[1:]))
# 	lines = np.array(list(map(lambda x : (float(x[0]), float(x[1])), lines)))
# 	lines /= np.pi
# 	lines *= 180
# 	times = np.expand_dims(np.arange(0, len(lines)) / frequency, axis=-1)
# 	return np.concatenate([lines, times], axis=-1)

def parseGPSMatrice100(rawGPSData):
	rawGPSData = StringIO("".join(rawGPSData))
	a = pd.read_csv(rawGPSData)
	a["timestamp"] = a["timestamp"].apply(lambda x : round(x / 10**9, 2))
	a["latitude"] = a["latitude"].apply(lambda x : x / np.pi * 180)
	a["longitude"] = a["longitude"].apply(lambda x : x / np.pi * 180)
	return a

def getGpsData(inFile, gpsFormat):
	f = open(inFile, "r", encoding = "ISO-8859-1")
	lines = f.readlines()

	if gpsFormat == "standard_gnrmc":
		gpsData = parseGPSDataGNRMC(lines)
	elif gpsFormat == "standard_gpgga":
		gpsData = parseGPSDataGPGGA(lines)
	elif gpsFormat == "standard_gngga":
		lines = list(filter(lambda x : x.startswith("$GNGGA"), lines))
		gpsData = parseGPSDataGNGGA(lines)
	elif gpsFormat == "matrice_100_logs_nots":
		gpsData = parseGPSMatrice100NoTs(lines, args.frequency)
	elif gpsFormat == "matrice_100_logs":
		for i in range(len(lines)):
			if lines[i].startswith("timestamp,latitude"):
				break
		lines = lines[i :]

		gpsData = parseGPSMatrice100(lines)
	f.close()
	return gpsData

def main():
	args = getArgs()

	gpsData = getGpsData(args.in_file, args.gps_format)
	print(gpsData)
	frequency = np.median(1 / (gpsData[1 : , 2] - gpsData[0 : -1, 2]))
	print("GPS frequency:", frequency)
	print(gpsData.shape)
	print("Saving to", args.out_file + ".npy")
	np.save(args.out_file, gpsData)

	if args.export_kml:
		kml = getKML(gpsData[:, 0 : 2])
		kmlFile = args.out_file + ".kml"
		g = open(kmlFile, "w")
		print("Saving to", kmlFile)
		g.write(kml)

	if args.export_osm:
		plt.plot(gpsData[..., 1], gpsData[..., 0])
		mplleaflet.save_html(fileobj=args.out_file + ".html")
		print("Saving to", args.out_file + ".html")

if __name__ == "__main__":
	main()