if [ $# -lt 3 ]; then
	echo "Usage: ./concatenate_vids.sh vid1.mp4 vid2.mp4 [vid3.mp4, ...] out.mp4";
	exit 0;
fi

i=0
concatStr="concat:"
for var in "$@"; do
	i=$(($i+1))

	if [ "$i" -eq "$#" ]; then
		break
	fi

	echo "$i/$#"
	concatStr="$concatStr""intermediate$i.ts|"
	ffmpeg -i $var -c copy -bsf:v h264_mp4toannexb -f mpegts intermediate$i.ts
done

outFile=$var
concatStr=${concatStr::-1}
echo "Merging together $concatStr into $outFile"
ffmpeg -i $concatStr -c copy -bsf:a aac_adtstoasc $outFile
rm *.ts