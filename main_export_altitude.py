import sys
import numpy as np
from argparse import ArgumentParser

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("in_file")
	parser.add_argument("out_file")
	parser.add_argument("gps_format")

	args = parser.parse_args()
	assert args.gps_format in ("matrice_100_logs", "standard_gpgga")
	return args

def parseGPSMatrice100(rawGPSData):
	lines = map(lambda x : x.split(","), rawGPSData[1:])
	lines = list(map(lambda x : (x[0], x[3]), lines))

	# Assume altitude like: 61.990971
	altitudes = np.expand_dims(np.array(list(map(lambda x : float(x[1]), lines))), axis=-1)

	# Assume timestamp like: 1540308809120298288
	times = np.expand_dims(np.array(list(map(lambda x : float(x[0][0:12]) / 100, lines))), axis=-1)

	return np.concatenate([altitudes, times], axis=-1)

def parseGPSDataGPGGA(rawGPSData):
	lines = list(filter(lambda x : x.startswith("$GPGGA"), rawGPSData))
	splitLines = list(map(lambda x : x.split(","), lines))
	splitLines = list(filter(lambda x : len(x) == 15 and x[9] != "", splitLines))
	splitLines = list(map(lambda x : (x[1], x[9]), splitLines))
	print(splitLines[0:10])

	newLines = []
	for i in range(len(splitLines)):
		line = splitLines[i]
		time, alt = line
		newLines.append((float(alt), float(time)))
	newLines = np.array(newLines)
	return newLines

def main():
	args = getArgs()

	f = open(args.in_file, "r", encoding = "ISO-8859-1")
	lines = f.readlines()
	if args.gps_format == "matrice_100_logs":
		gpsData = parseGPSMatrice100(lines)
	elif args.gps_format == "standard_gpgga":
		gpsData = parseGPSDataGPGGA(lines)

	print(gpsData)
	frequency = np.median(1 / (gpsData[1 : , 1] - gpsData[0 : -1, 1]))
	print("GPS frequency:", frequency)
	print(gpsData.shape)
	print("Saving to", args.out_file + ".npy")
	np.save(args.out_file, gpsData)

if __name__ == "__main__":
	main()