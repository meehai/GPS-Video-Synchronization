import h5py
import numpy as np
from neural_wrappers.readers import DatasetReader
from neural_wrappers.transforms import Transformer

class PoliFlightReader(DatasetReader):
	def __init__(self, datasetPath):
		super().__init__(datasetPath, allDims=["rgb", "gps"], dataDims=["rgb"], labelDims=["gps"], \
			dimTransform={"rgb": lambda x : np.float32(x)}, augTransform=[], resizer={}, \
			normalizer={"rgb" : "min_max_normalization", "gps" : "min_max_normalization"})
		self.dataset = h5py.File(self.datasetPath, "r")
		self.numData = {item : len(self.dataset[item]["rgb"]) for item in self.dataset}

		self.minimums = {
			"rgb" : np.array([0, 0, 0]),
			"gps": np.array([44.43448468573079, 26.045496832850702])
		}

		self.maximums = {
			"rgb" : np.array([255, 255, 255]),
			"gps" : np.array([44.436176574351016, 26.048940957214707])
		}

		print("[PoliFlightReader Reader] Setup complete")

	def iterate_once(self, type, miniBatchSize):
		assert type in ("train", "validation")

		dataset = self.dataset[type]
		numIterations = self.getNumIterations(type, miniBatchSize, accountTransforms=False)

		for i in range(numIterations):
			startIndex = i * miniBatchSize
			endIndex = min((i + 1) * miniBatchSize, self.numData[type])
			assert startIndex < endIndex, "startIndex < endIndex. Got values: %d %d" % (startIndex, endIndex)
			numData = endIndex - startIndex

			for items in self.getData(dataset, startIndex, endIndex):
				data, labels = items
				yield data, labels[..., 0 : 2]

def computeDotMap(labels, gridSize=(190, 270), radius=7.5):
	MB = labels.shape[0]
	labels = labels * gridSize
	labels = np.int32(np.round(labels))

	newLabels = np.zeros((MB, *gridSize), dtype=np.float32)
	l, r = labels[..., 0], labels[..., 1]
	Y, X = np.ogrid[0 : gridSize[0], 0 : gridSize[1]]
	for i in range(MB):
		dist_from_center = np.sqrt((Y - l[i])**2 + (X - r[i])**2)
		mask = dist_from_center <= radius
		newLabels[i] = mask
	return newLabels

class PoliFlightDotReader(PoliFlightReader):
	def __init__(self, datasetPath):
		super().__init__(datasetPath)
		self.grid = (190, 270)

	def iterate_once(self, type, miniBatchSize):
		generator = super().iterate_once(type, miniBatchSize)
		for items in generator:
			data, labels = items
			newLabels = computeDotMap(labels, gridSize=self.grid, radius=7.5)
			yield data, newLabels