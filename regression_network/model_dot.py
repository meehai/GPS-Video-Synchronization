import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from neural_wrappers.pytorch import NeuralNetworkPyTorch

class UNetBlock(NeuralNetworkPyTorch):
	def __init__(self, dIn, dOut, kernel1_size=(3, 3), kernel2_size=(3, 3), padding=0):
		super(UNetBlock, self).__init__()
		self.conv1 = nn.Conv2d(in_channels=dIn, out_channels=dOut, kernel_size=kernel1_size, padding=padding)
		self.conv2 = nn.Conv2d(in_channels=dOut, out_channels=dOut, kernel_size=kernel2_size, padding=padding)

	def forward(self, x):
		out1 = F.relu(self.conv1(x))
		out2 = F.relu(self.conv2(out1))
		return out2

class ConcatenateBlock(NeuralNetworkPyTorch):
	def __init__(self, dIn, dOut, kernel_size=(3, 3)):
		super().__init__()
		self.convt = nn.ConvTranspose2d(in_channels=dIn, out_channels=dOut, kernel_size=kernel_size, stride=(2, 2))

	def forward(self, x_down, x_up):
		y_up = F.relu(self.convt(x_up))
		y_up = F.pad(y_up, (0, -1, 0, -1))
		x_down_interp = F.interpolate(x_down, (y_up.shape[2], y_up.shape[3]))
		# print("Interpolating:", x_down.shape, "=>", x_down_interp.shape)
		# print("Concating:", x_down_interp.shape, y_up.shape)
		y_concat = tr.cat([x_down_interp, y_up], dim=1)
		return y_concat

class ModelDot(NeuralNetworkPyTorch):
	def __init__(self, numFilters):
		super().__init__()

		# Feature extractor part (down)
		self.downBlock1 = UNetBlock(dIn=3, dOut=numFilters, padding=(1, 0))
		self.pool1 = nn.MaxPool2d(kernel_size=2)
		self.downBlock2 = UNetBlock(dIn=numFilters, dOut=numFilters * 2, padding=(1, 0))
		self.pool2 = nn.MaxPool2d(kernel_size=2)
		self.downBlock3 = UNetBlock(dIn=numFilters * 2, dOut=numFilters * 4, padding=(1, 0))
		self.pool3 = nn.MaxPool2d(kernel_size=2)
		self.downBlock4 = UNetBlock(dIn=numFilters * 4, dOut=numFilters * 8, padding=(1, 0))
		self.pool4 = nn.MaxPool2d(kernel_size=2)

		self.bottleneck1 = nn.Conv2d(in_channels=numFilters * 8, out_channels=numFilters * 8, kernel_size=(3, 3), padding=(1, 0))
		self.bottleneck2 = nn.Conv2d(in_channels=numFilters * 8, out_channels=numFilters * 8, kernel_size=(3, 3), padding=(1, 0))
		self.bottleneck3 = nn.Conv2d(in_channels=numFilters * 8, out_channels=numFilters * 8, kernel_size=(3, 3), padding=(1, 0))
		self.bottleneck4 = nn.Conv2d(in_channels=numFilters * 8, out_channels=numFilters * 8, kernel_size=(3, 3), padding=(1, 0))
		self.bottleneck5 = nn.Conv2d(in_channels=numFilters * 8, out_channels=numFilters * 8, kernel_size=(3, 3), padding=(1, 0))
		self.bottleneck6 = nn.Conv2d(in_channels=numFilters * 8, out_channels=numFilters * 8, kernel_size=(3, 3), padding=(2, 2))

		# Final up-sample layers
		self.upBlock4 = UNetBlock(dIn=numFilters * 8, dOut=numFilters * 8, padding=(1, 1))
		self.up3 = ConcatenateBlock(dIn=numFilters * 8, dOut=numFilters * 8)
		self.upBlock3 = UNetBlock(dIn=numFilters * 16, dOut=numFilters * 4, kernel1_size=(2, 4), padding=(1, 1))
		self.up2 = ConcatenateBlock(dIn=numFilters * 4, dOut=numFilters * 4)

		self.finalConv = nn.Conv2d(in_channels=numFilters * 8, out_channels=1, kernel_size=(1, 1))

	def forward(self, x):
		x = tr.transpose(tr.transpose(x, 1, 3), 2, 3)

		y_down1 = self.downBlock1(x)
		y_down1pool = self.pool1(y_down1)
		y_down2 = self.downBlock2(y_down1pool)
		y_down2pool = self.pool2(y_down2)
		y_down3 = self.downBlock3(y_down2pool)
		y_down3pool = self.pool3(y_down3)
		y_down4 = self.downBlock4(y_down3pool)
		y_down4pool = self.pool4(y_down4)

		# print("x:", x.shape)
		# print("y_down1", y_down1.shape)
		# print("y_down1pool", y_down1pool.shape)
		# print("y_down2", y_down2.shape)
		# print("y_down2pool", y_down2pool.shape)
		# print("y_down3", y_down3.shape)
		# print("y_down3pool", y_down3pool.shape)
		# print("y_down4", y_down4.shape)
		# print("y_down4pool", y_down4pool.shape)

		# print("------------------")

		y_bottleneck1 = self.bottleneck1(y_down4pool)
		y_bottleneck2 = self.bottleneck2(y_bottleneck1)
		y_bottleneck3 = self.bottleneck3(y_bottleneck2)
		y_bottleneck4 = self.bottleneck4(y_bottleneck3)
		y_bottleneck5 = self.bottleneck5(y_bottleneck4)
		y_bottleneck6 = self.bottleneck6(y_bottleneck5)
		# print("y_bottleneck1", y_bottleneck1.shape)
		# print("y_bottleneck2", y_bottleneck2.shape)
		# print("y_bottleneck3", y_bottleneck3.shape)
		# print("y_bottleneck4", y_bottleneck4.shape)
		# print("y_bottleneck5", y_bottleneck5.shape)
		# print("y_bottleneck6", y_bottleneck6.shape)

		# print("------------------")

		y_up4block = self.upBlock4(y_bottleneck6)
		# print("y_up4block", y_up4block.shape)
		y_up3 = self.up3(y_down4, y_up4block)
		# print("y_up3", y_up3.shape)
		y_up3block = self.upBlock3(y_up3)
		# print("y_up3block", y_up3block.shape)
		y_up2 = self.up2(y_down3, y_up3block)
		# print("y_up2", y_up2.shape)

		y_final = F.sigmoid(self.finalConv(y_up2))
		# print("y_final", y_final.shape)
		y_final = y_final.view(y_final.shape[0], y_final.shape[2], y_final.shape[3])

		return y_final