import torch as tr
import torch.nn.functional as F
import numpy as np

def regression_loss(y, t):
	return tr.sum((y - t)**2)

def np_dice_coeff(y, t, **k):
	smooth = 1.
	y_f = y.flatten()
	t_f = t.flatten()
	intersection = np.sum(t_f * y_f)
	score = (2. * intersection + smooth) / (np.sum(t_f) + np.sum(y_f) + smooth)
	return score

def dice_coeff(y, t, **k):
	smooth = 1.
	y_f = y.flatten()
	t_f = t.flatten()
	intersection = tr.sum(t_f * y_f)
	score = (2. * intersection + smooth) / (tr.sum(t_f) + tr.sum(y_f) + smooth)
	return score

def dice_loss(y, t):
	loss = 1 - dice_coeff(y, t)
	return loss

def bce_dice_loss(y, t):
	loss1 = F.binary_cross_entropy(y, t)
	loss2 = dice_loss(y, t)
	return loss1 + loss2

def meter_metric_lat(y, t, **k):
	return 190 * np.mean(np.abs(y - t)[..., 0])

def meter_metric_lon(y, t, **k):
	return 270 * np.mean(np.abs(y - t)[..., 1])