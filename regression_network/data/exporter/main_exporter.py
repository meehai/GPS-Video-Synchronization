import pims
import numpy as np
import sys
import h5py
from argparse import ArgumentParser

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("in_video")
	parser.add_argument("in_gps")
	parser.add_argument("out_dataset")

	parser.add_argument("--offset_start", type=int, default=None)
	parser.add_argument("--offset_end", type=int, default=None)

	args = parser.parse_args()
	return args

def main():
	args = getArgs()
	video = pims.Video(args.in_video)
	gps = np.load(args.in_gps)
	file = h5py.File(args.out_dataset, "w")

	args.offset_start = 0 if not args.offset_start else args.offset_start
	args.offset_end = len(gps) if not args.offset_end else args.offset_end
	gps = gps[args.offset_start : args.offset_end, 0 : 2]

	perm = np.random.permutation(len(gps))
	numTrain = int(0.8 * len(gps))
	numVal = len(gps) - numTrain

	file.create_group("train")
	file.create_group("validation")
	file["train"].create_dataset("rgb", (numTrain, *video.frame_shape), dtype=np.uint8)
	file["train"].create_dataset("gps", (numTrain, 2), dtype=np.float32)
	file["validation"].create_dataset("rgb", (numVal, *video.frame_shape), dtype=np.uint8)
	file["validation"].create_dataset("gps", (numVal, 2), dtype=np.float32)

	for i in range(numTrain):
		if i % 100 == 0:
			print("%d/%d done (train)" % (i, numTrain))
		file["train"]["rgb"][i] = video[perm[i]]
		file["train"]["gps"][i] = gps[perm[i]]

	for i in range(numVal):
		if i % 100 == 0:
			print("%d/%d done (val)" % (i, numVal))
		file["validation"]["rgb"][i] = video[perm[numTrain + i]]
		file["validation"]["gps"][i] = gps[perm[numTrain + i]]

	file.flush()
	print("Done")

if __name__ == "__main__":
	main()