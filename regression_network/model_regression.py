from neural_wrappers.pytorch import NeuralNetworkPyTorch
from neural_wrappers.models import SqueezeNetNoTop
import torch as tr
import torch.nn.functional as F
import torch.nn as nn

class ModelRegression(NeuralNetworkPyTorch):
	def __init__(self):
		super().__init__()
		self.baseType = SqueezeNetNoTop()
		self.conv1 = nn.Conv2d(in_channels=512, out_channels=64, kernel_size=3)
		self.fc1 = nn.Linear(64 * 42 * 77, 100)
		self.fc2 = nn.Linear(100, 2)

		# Freeze params for base model
		for param in self.baseType.parameters():
			param.requires_grad_(False)

	def forward(self, x):
		x = tr.transpose(tr.transpose(x, 1, 3), 2, 3)
		y_base = self.baseType.forward(x)

		y1 = F.relu(self.conv1(y_base)).view(-1, 64 * 42 * 77)
		y2 = F.relu(self.fc1(y1))
		y3 = self.fc2(y2)
		return y3